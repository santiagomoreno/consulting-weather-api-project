import React, {Component} from 'react';
import PropTypes from'prop-types';
import LocationList from "../components/LocationList";
import {connect} from "react-redux";
import {setSelectedCity, setWeather} from '../actions';
import {getWeatherCities} from '../reducers';

class LocationListContainer extends Component{

    componentDidMount() {
        this.props.dispatchSetWeather(this.props.cities)
    }

    handleSelectedLocation = city =>(this.props.dispatchSetCity(city));

    render() {
        return (
            <LocationList
                cities={this.props.citiesWeather}
                onSelectedLocation={this.handleSelectedLocation}
            />
        );
    }
}

LocationListContainer.propTypes ={
    dispatchSetCity: PropTypes.func.isRequired,
    cities: PropTypes.array.isRequired,
    citiesWeather: PropTypes.array.isRequired,
};
const mapDispatchToProps= dispatch =>({
    dispatchSetCity: value => value && dispatch(setSelectedCity(value)),
    dispatchSetWeather: cities => cities && dispatch(setWeather(cities)),
});

const mapStateToProps = state =>({
    citiesWeather: getWeatherCities(state),
});

export default  connect(mapStateToProps, mapDispatchToProps)(LocationListContainer);