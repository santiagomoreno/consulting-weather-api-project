import {CLOUD, THUNDER, DRIZZLE, SNOW, SUN, RAIN} from "../constants/weathers";


const getWeatherState= weather_data =>{
    const id= weather_data.weather[0].id;

    if(id<300) return THUNDER;
    else if(id<400) return DRIZZLE;
    else if(id<500) return RAIN;
    else if(id<700) return SNOW;
    else if(id===800) return SUN;
    return CLOUD;
}
const transformWeather = weather_data =>{
    const {humidity, temp} = weather_data.main;
    const {speed} = weather_data.wind;
    const weatherState = getWeatherState(weather_data);

    const data = {
        humidity,
        temperature: ~~temp,
        wind: speed,
        weatherState
    }

    return data;
};

export default transformWeather;