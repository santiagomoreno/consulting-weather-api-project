import {url_base_weather, api_key} from "../constants/api_url";


const getWeatherUrlByCity = city =>{
    return url_base_weather+"?q="+city+"&units=metric&appid="+api_key;

}

export default getWeatherUrlByCity;