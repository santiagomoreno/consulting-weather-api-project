import {combineReducers} from "redux";
import {city} from './city';
import {cities, getForecastDataFromCities as _getForecastDataFromCities,
    getWeatherCities as _getWeatherCities} from './cities';


export default combineReducers({
    city,
    cities,
});

export const getForecastDataFromCities = state => ( _getForecastDataFromCities( state.cities, getCity(state)) );
export const getCity = state =>state.city;
export const getWeatherCities= state => (_getWeatherCities(state.cities));
