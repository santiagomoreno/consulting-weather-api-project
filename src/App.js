import React, {Component} from 'react';
import './App.css';
import Paper from '@material-ui/core/Paper';
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import LocationListContainer from "./containers/LocationListContainer";
import {Grid, Col, Row} from 'react-flexbox-grid';
import ForecastExtendedContainer from "./containers/ForecastExtendedContainer";


const cities = [
    'Buenos Aires,ar',
    'Washington,us',
    'Bogota,co',
    'Ciudad De Mexico,mx',
    'Lima,pe',
]

class App extends Component{

    render() {
        return (
                <Grid className="grid">
                    <Row>
                        <AppBar position='sticky'>
                            <Toolbar>
                                <Typography color='inherit'>
                                    Weather World
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <LocationListContainer
                                cities={cities}
                            />
                        </Col>
                        <Col xs={12} md={6}>
                            <Paper className="paper">
                                <div className="details">
                                    <ForecastExtendedContainer/>
                                </div>
                            </Paper>
                        </Col>
                    </Row>
                </Grid>
        );
    }
}

export default App;