import React from 'react';
import PropTypes from 'prop-types';
import WeatherData from "../WeatherLocation/WeatherData/index";
import './style.css';


const ForecastItem = ({weekday, hour, data}) => {
    return(
        <div>
            <div className={"day-hour"}>{weekday} - {hour} hs</div>
            <WeatherData data={data}/>
        </div>
    )
};

ForecastItem.propTypes ={
    weekday: PropTypes.string.isRequired,
    hour: PropTypes.number.isRequired,
    data: PropTypes.shape({
        temperature: PropTypes.number.isRequired,
        weatherState: PropTypes.string.isRequired,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.number.isRequired,
    }),
}

export default ForecastItem;