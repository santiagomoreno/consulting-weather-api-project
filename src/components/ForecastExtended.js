import React from 'react';
import PropTypes from "prop-types";
import './style.css';
import ForecastItem from "./ForecastItem/index";


const renderForecastItemDay =( forecastData )=>{
    return forecastData.map(forecast => (
        <ForecastItem
            key={forecast.weekDay+forecast.hour}
            weekday={forecast.weekDay}
            hour={forecast.hour}
            data={forecast.data}
        />));
}

const renderProgress=()=>{
    return <h3 className={"loading"}>Cargando Pronostico Extendido...</h3>;
}

const ForecastExtended=({ city, forecastData })=>(
    <div>
        <h2 className='forecast-title'>
            <div className="title-forecast">Forecast Extended for {city}</div>
        </h2>
        {forecastData ?
            renderForecastItemDay(forecastData):
            renderProgress()}
    </div>
)


ForecastExtended.propTypes = {
    city: PropTypes.string.isRequired,
    forecastData:PropTypes.array,
    data: PropTypes.shape({
        temperature: PropTypes.number.isRequired,
        weatherState: PropTypes.string.isRequired,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.number.isRequired,
    }),
}

export default ForecastExtended;