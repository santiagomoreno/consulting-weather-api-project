import React from "react";
import WeatherIcons from "react-weathericons";
import PropTypes from 'prop-types';
import './style.css';


const icons = {
    sun:"day-sunny",
    fog: "day-fog",
    cloud:"cloud",
    rain:"rain",
    snow:"snow",
    thunder: "day-thunderstorm",
    drizzle: "day-showers",
};

const getWeatherIcon = weatherState =>{
    const icon = icons[weatherState];

    if(icon)
        return <WeatherIcons className="weatherIcons" name={icon} size="4x"/>;
    else
        return <WeatherIcons className="weatherIcons" name={"day-sunny"} size="4x"/>;
};

const WeatherTemperature = (props) =>{
    const {temperature, weatherState} = props;

    return(
        <div className="weatherTemperature">
            {getWeatherIcon(weatherState)}
            &nbsp;
            <span className="temperature">{temperature}</span>
            <span className="temperatureType">  C°</span>
        </div>
    );
};

WeatherTemperature.propTypes ={
    temperature: PropTypes.number.isRequired,
    weatherState: PropTypes.string.isRequired,

}

export default WeatherTemperature;