import React from 'react';
import WeatherData from "./WeatherData";
import Location from "./Location";
import PropTypes from "prop-types";
import './style.css';
import {CircularProgress} from "@material-ui/core";

const WeatherLocation = ({onWeatherLocationClick, city, data}) => (

    <div className="weatherLocationContainer" onClick={onWeatherLocationClick}>
        <Location city={city}/>
        {data ?
            <WeatherData data={data}/>
            :
            <CircularProgress size={50}/>
        }
    </div>
);

WeatherLocation.propType = {
    city: PropTypes.string,
    onWeatherLocationClick: PropTypes.func,
    data: PropTypes.shape({
        temperature: PropTypes.number.isRequired,
        weatherState: PropTypes.string.isRequired,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.number.isRequired,
    }),
}

export default WeatherLocation;