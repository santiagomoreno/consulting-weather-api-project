import transformForecast from "../services/transformForecast";
import transformWeather from "../services/transformWeather";
import getWeatherUrlByCity from "../services/getUrlWeatherByCity";

export const SET_CITY='SET_CITY';
export const SET_FORECAST_DATA='SET_FORECAST_DATA';
export const SET_WEATHER_CITY='SET_WEATHER_CITY';
export const GET_WEATHER_CITY='GET_WEATHER_CITY';

export const setCity= payload =>({type: SET_CITY, payload});
export const setForecastData = payload =>({type:SET_FORECAST_DATA, payload});
export const setWeatherCity= payload =>({type: SET_WEATHER_CITY, payload});
export const getWeatherCity= payload =>({type: GET_WEATHER_CITY, payload});

const api_key="62792dc04becb681426dd55b4b63323b";
const url="http://api.openweathermap.org/data/2.5/forecast";


export const setSelectedCity = payload => {
    return ( dispatch, getState )=> {
        const url_forecast= url+"?q="+payload+"&units=metric&appid="+api_key;

        dispatch(setCity(payload));

        const state = getState();
        const date= state.cities[payload] && state.cities[payload].forecastDataDate;

        const now =new Date();

        //si pasa menos de un 1 min del ultimo llamado, no se ejecuta el set_forecast_data
        if(date && (now-date)< 1 * 60 * 10000) return;

        return fetch(url_forecast).then(
            data => (data.json()))
            .then(
                weather_data => {
                    const forecastData= transformForecast(weather_data);
                    dispatch(setForecastData({city:payload, forecastData}))
                },
            );
    };
};

export const setWeather = payload => {
    return dispatch => {
        payload.forEach(city =>{
            dispatch(getWeatherCity(city))

            fetch(getWeatherUrlByCity(city)).then(resolve => {
                    return resolve.json();
                }).then(data => {
                        const weather = transformWeather(data);
                        dispatch(setWeatherCity({city, weather}));
                })
            })
    }
};